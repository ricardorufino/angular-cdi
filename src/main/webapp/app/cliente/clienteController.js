var app = angular.module("cliente", []);

app.directive("cpfDir", function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function ($scope, element, attrs, ctrl) {            
            $(element).mask("999.999.999-99");
            element.on('blur', function () {
                console.log(element.val());

                strCPF = element.val();
                strCPF = strCPF.replace(/\./g, "");
                strCPF = strCPF.replace(/\-/g, "");
                console.log(strCPF);
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF === "00000000000") {
                    alert("Invalido");
                    element.val("");
                    return;
                }
                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto === 10) || (Resto === 11))
                    Resto = 0;
                if (Resto !== parseInt(strCPF.substring(9, 10))) {
                    alert("Invalido");
                    element.val("");
                    return;
                }
                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto === 10) || (Resto === 11)) {
                    Resto = 0;
                }
                if (Resto !== parseInt(strCPF.substring(10, 11))) {
                    alert("Invalido");
                    element.val("");
                }
                ctrl.$setViewValue(strCPF);
            });
        }
    };
});




app.controller("clienteControler", function ($scope, $http) {

    $http.get("/local/rest/cliente/listagem")
            .then(function (response) {
                $scope.listagem = response.data;
            });

    $scope.salvar = function () {
//        if ($('#cpf').validateCPF()) {
        $http.post("/local/rest/cliente/salvar", $scope.cliente).then(
                function () {
                    delete $scope.cliente;
                    $http.get("/local/rest/cliente/listagem")
                            .then(function (response) {
                                $scope.listagem = response.data;
                            });
                });
//        }else{
//            alert("CPF invalido");
//        }
    };

    $scope.alterar = function (cliente) {
        $scope.cliente = angular.copy(cliente);
    };

    $scope.excluir = function (cliente) {
        retorno = confirm("Deseja excluir?");
        if (retorno) {
            $http.post("/local/rest/cliente/excluir", cliente).then(
                    function () {
                        $http.get("/local/rest/cliente/listagem")
                                .then(function (response) {
                                    $scope.listagem = response.data;
                                });
                    });
        }
    };

});