package br.com.controle;

import br.com.entidade.Cliente;
import br.com.entidade.Produto;
import br.com.entidade.Venda;
import br.com.service.ClienteService;
import br.com.service.ProdutoService;
import br.com.service.VendaService;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(value = "/venda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VendaController implements Serializable {

    @Inject
    private VendaService vendaService;
    @Inject
    private ClienteService clienteService;
    @Inject
    private ProdutoService produtoService;

    @GET
    @Path("/getCliente/{query}")
    public List<Cliente> getClienteAutocomplete(@PathParam("query") String nome) {
        return clienteService.clienteAutoComplete(nome);
    }

    @GET
    @Path("/getProduto/{query}")
    public List<Produto> getProdutoAutocomplete(@PathParam("query") String nome) {
        return produtoService.produtoAutoComplete(nome);
    }

    @GET
    @Path("/listagem")
    public List<Venda> listagem() {
        return vendaService.listar();
    }

    @POST
    @Path("/salvar")
    public void salvar(Venda v) throws Exception {
        vendaService.salvar(v);
    }

}
