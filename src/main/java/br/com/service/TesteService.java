/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.service;

import br.com.entidade.Teste;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import br.com.persistencia.Transacional;

/**
 *
 * @author ricardo
 */
@Transacional
public class TesteService implements Serializable {

    @Inject
    private EntityManager em;

    public void salvar(Teste t) {
        em.merge(t);
    }

    public void excluir(Teste t) {
        em.remove(em.merge(t));
    }

    public List<Teste> listar() {
        return em.createQuery("FROM Teste").getResultList();
    }
}
