package br.com.service;

import br.com.entidade.Cliente;
import br.com.persistencia.Transacional;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author barizon
 */
@Transacional
public class ClienteService extends AbstractService<Cliente> implements Serializable {

    @Inject
    public EntityManager em;

    public ClienteService() {
        super(Cliente.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    public List<Cliente> clienteAutoComplete(String nome) {
        return em.createQuery("FROM Cliente AS c WHERE LOWER(c.nome) LIKE '%" + nome.toLowerCase() + "%'").getResultList();
    }
}
